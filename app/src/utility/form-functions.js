/**
 * Function is responsible for handling state changes from
 * form elements
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const handleChange = (event, component) => component.setState({[event.target.id]: event.target.value});

/**
 * Function is responsible for cleaning up form validation messages.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const cleanupFormMessages = () => {
    const removeMessages = (elements) => Array.prototype.forEach.call(elements, node => node.parentNode.removeChild(node));
    removeMessages(document.querySelectorAll("div.invalid"));
    removeMessages(document.querySelectorAll("div.valid"));
};

/**
 * Function is responsible for fetching all input elements from a
 * provided form object.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const fetchFormInputs = (form) => {
    const formInputs = [];
    Array.prototype.forEach.call(form.elements, element => {
        if (element.tagName && element.tagName.toUpperCase() === "INPUT")
            formInputs.push([element.id.toString(), element])
    });
    return formInputs;
};

/**
 * Function is responsible for constructing a single form validation
 * message representing an error.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const constructErrorMessage = (errorString) => {
    const errorDiv = document.createElement("div");
    errorDiv.innerHTML = "<small>" + errorString + "</small>";
    errorDiv.className = "invalid";
    return errorDiv
};

/**
 * Function is responsible for constructing a single form validation
 * message representing as success.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const constructSuccessMessage = (successString) => {
    const successDiv = document.createElement("div");
    successDiv.innerHTML = "<small>" + successString + "</small>";
    successDiv.className = "valid";
    return successDiv
};

/**
 * Function is responsible for constructing a generic missing field
 * validation error.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const missingFieldMessage = (fieldName) => "Please provide a valid " + fieldName + ".";