import axios from "axios"
import {constructErrorMessage, fetchFormInputs} from "./form-functions";
import {FailureAlert} from "../view/component/DismissibleAlert";
import React from "react";

const RMW_BASE_ENDPOINT = "http://127.0.0.1:9000/oms/rmw";

/**
 * Object is responsible for storing all application
 * endpoints.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const RmwEndpoints = {
    Urm: {
        REST_AUTHENTICATION_TOKEN: RMW_BASE_ENDPOINT + "/urm/user/authenticate/reset",
        AUTHENTICATE: RMW_BASE_ENDPOINT + "/urm/user/authenticate",

        NEW_CUSTOMER: RMW_BASE_ENDPOINT + "/urm/customer",
        VIEW_CUSTOMER_BY_AUTH_TOKEN: RMW_BASE_ENDPOINT + "/urm/customer/view",
        UPDATE_CUSTOMER: (customerId) => RMW_BASE_ENDPOINT + "/urm/customer/" + customerId + "/update",
        REMOVE_CUSTOMER: (customerId) => RMW_BASE_ENDPOINT + "/urm/customer/" + customerId + "/delete",

        NEW_ADDRESS: RMW_BASE_ENDPOINT + "/urm/address",
        REMOVE_ADDRESS: (addressId) => RMW_BASE_ENDPOINT + "/urm/address/" + addressId + "/delete",
        LIST_ADDRESS_BY_CUSTOMER_ID: (customerId) => RMW_BASE_ENDPOINT + "/urm/address/" + customerId + "/list",
        ASSIGN_ACTIVE_ADDRESS: (addressId) => RMW_BASE_ENDPOINT + "/urm/address/" + addressId + "/default",
        VIEW_ACTIVE_ADDRESS: (customerId) => RMW_BASE_ENDPOINT + "/urm/address/" + customerId + "/view/default",

        NEW_PAYMENT_DETAIL: RMW_BASE_ENDPOINT + "/urm/payment-detail",
        DELETE_PAYMENT_DETAIL: (id) => RMW_BASE_ENDPOINT + "/urm/payment-detail/" + id + "/delete",
        LIST_PAYMENT_DETAIL: (customerId) => RMW_BASE_ENDPOINT + "/urm/payment-detail/" + customerId + "/list",
        ASSIGN_ACTIVE_PAYMENT_DETAIL: (id) => RMW_BASE_ENDPOINT + "/urm/payment-detail/" + id + "/default",
        VIEW_ACTIVE_PAYMENT_DETAIL: (customerId) => RMW_BASE_ENDPOINT + "/urm/payment-detail/" + customerId + "/view/default"
    },

    Om: {
        NEW_ORDER: RMW_BASE_ENDPOINT + "/om/order",
        VIEW_ORDER: (orderId) => RMW_BASE_ENDPOINT + "/om/order/" + orderId + "/view",
        LIST_ORDER_BY_CUSTOMER: (customerId) => RMW_BASE_ENDPOINT + "/om/order/" + customerId + "/list/customer",

        LIST_MENU_CATEGORY: RMW_BASE_ENDPOINT + "/om/menu-category/list",

        LIST_MENU_ITEM: (menuCategoryId) => RMW_BASE_ENDPOINT + "/om/menu-item/" + menuCategoryId + "/list/menu_category",
        VIEW_MENU_ITEM: (menuItemId) => RMW_BASE_ENDPOINT + "/om/menu-item/" + menuItemId + "/view",

        LIST_MENU_OPTIONS: (menuOptionIds) => RMW_BASE_ENDPOINT + "/om/menu-item-option/" + menuOptionIds + "/list",

        LIST_MENU_OPTION_GROUPS: (menuOptionGroupIds) => RMW_BASE_ENDPOINT + "/om/menu-item-option-group/" + menuOptionGroupIds + "/list",

        VIEW_DISCOUNT_CODE: (discountCode) => RMW_BASE_ENDPOINT + "/om/discount-code/" + discountCode + "/view/code",

        VIEW_DELIVERY_OPTION: RMW_BASE_ENDPOINT + "/om/delivery-option/view"
    }
};

/**
 * Object is responsible for providing request headers.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const requestHeaders = {"Content-Type": "application/json; charset=utf-8;"};

/**
 * Object is responsible for providing authentication request
 * headers.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const authenticationHeaders = {
    "Content-Type": "application/json; charset=utf-8;",
    "Authorization": localStorage.getItem("oms.authentication.key") !== null ? JSON.parse(localStorage.getItem("oms.authentication.key"))["token"] : null
};

/**
 * Function is responsible for updating the currently stored
 * authentication token.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const resetAuthenticationToken = () => axios
    .post(RmwEndpoints.Urm.REST_AUTHENTICATION_TOKEN, null, {headers: authenticationHeaders})
    .then(response => localStorage.setItem("oms.authentication.key", JSON.stringify({
        token: response.data,
        time: new Date().getTime()
    })));

/**
 * Function is responsible for sending a POST request to the provided
 * endpoint with the given JSON payload.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const requestPost = (endpoint, payload, onSuccess, form) => {
    axios
        .post(endpoint, payload, {headers: requestHeaders})
        .then(response => onSuccess(response))
        .catch(failure => handleValidationFaults(failure, form));
};

/**
 * Function is responsible for sending authenticated POST requests to the
 * provided endpoint with the given JSON payload.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const authPost = (endpoint, payload, onSuccess, form) => {
    axios
        .post(endpoint, payload, {headers: authenticationHeaders})
        .then(response => onSuccess(response))
        .catch(failure => handleValidationFaults(failure, form));
};

/**
 * Function is responsible for sending authenticated PUT requests to the
 * provided endpoint with the given JSON payload.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const authPut = (endpoint, payload, onSuccess, form) => {
    axios
        .put(endpoint, payload, {headers: authenticationHeaders})
        .then(response => onSuccess(response))
        .catch(failure => handleValidationFaults(failure, form));
};

/**
 * Function is responsible for sending a GET request to the provided
 * endpoint with the given JSON payload.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const requestGet = (endpoint, onSuccess, onFailure = null) => {
    axios.get(endpoint, {headers: requestHeaders})
        .then(response => onSuccess(response))
        .catch(failure => onFailure !== null ? onFailure(failure) : null);
};

/**
 * Function is responsible for sending authenticated GET requests to the
 * provided endpoint with the given JSON payload.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const authGet = (endpoint, onSuccess, onFailure = null) => {
    axios.get(endpoint, {headers: authenticationHeaders})
        .then(response => onSuccess(response))
        .catch(failure => onFailure !== null ? onFailure(failure) : null);
};

/**
 * Function is responsible for sending authenticated DELETE requests to the
 * provided endpoint with the given JSON payload.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const authDelete = (endpoint, onSuccess, onFailure = null) => {
    axios.delete(endpoint, {headers: authenticationHeaders})
        .then(response => onSuccess(response))
        .catch(failure => onFailure !== null ? onFailure(failure) : null);
};

/**
 * Function is responsible for handling validation faults thrown
 * by the OMS ReactiveMiddleware applications.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const handleValidationFaults = (error, form) => {
    if (error.response != null) {
        if (error.response.status === 400 || error.response.status === 401) {
            fetchFormInputs(form).forEach(tuple => {
                if (Array.isArray(error.response.data)) {
                    error.response.data
                        .filter(vf => vf["field"] === tuple[0])
                        .map(vf => tuple[1].parentNode.appendChild(constructErrorMessage(vf["fault"])))
                } else if (tuple[0] === error.response.data["field"]) {
                    tuple[1].parentNode.appendChild(constructErrorMessage(error.response.data["fault"]))
                }
            });
        } else {
            this.setState({
                alert: <FailureAlert
                    messages="An unexpected error has occurred."
                    component={this}
                />
            })
        }
    }
};