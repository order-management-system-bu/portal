/**
 * Utility function adds a space before each capital letter and
 * uppercases the first letter in each word.
 */
export const beutifyString = (s) => s
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, function (str) {
        return str.toUpperCase();
    });