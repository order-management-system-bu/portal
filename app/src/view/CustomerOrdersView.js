import React from "react";
import CustomerControlNavbarWrapper from "./component/CustomerControlNavbarWrapper";
import {authGet, RmwEndpoints} from "../utility/request-handler";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Redirect from "react-router-dom/es/Redirect";

/**
 * Class is responsible for rendering the CustomerOrders view.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class CustomerOrdersView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            redirectUrl: null,

            orders: []
        }
    }

    componentDidMount() {
        authGet(RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN, (response) =>
            authGet(
                RmwEndpoints.Om.LIST_ORDER_BY_CUSTOMER(response.data["id"]),
                (response) => this.setState({
                    orders: response.data
                })
            )
        )
    }

    render() {
        const redirectToOrderSummary = (orderId) => this.setState({
            redirectUrl: "/order/summary/" + orderId
        });

        return (
            this.state.redirectUrl ? <Redirect to={this.state.redirectUrl}/> :
                <CustomerControlNavbarWrapper
                    activeKey="ordersView"
                    pageContent={
                        <div>
                            <h1>Your Orders</h1>
                            <hr/>
                            {this.state.orders.map(order =>
                                <Container>
                                    <Row>
                                        <Col xs={3}>
                                            <b>{order["id"]}</b>
                                        </Col>
                                        <Col xs={3}>
                                            {order["orderStatus"]}
                                        </Col>
                                        <Col xs={3}>
                                            {"£" + order["price"].toFixed(2)}
                                        </Col>
                                        <Col xs={3}>
                                            <Button type="button" onClick={() => redirectToOrderSummary(order["id"])}>
                                                Order Details
                                            </Button>
                                        </Col>
                                    </Row>
                                    <hr/>
                                </Container>
                            )}
                        </div>
                    }
                />
        )
    }
}