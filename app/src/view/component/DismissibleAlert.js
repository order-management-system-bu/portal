import React from "react";
import Alert from "react-bootstrap/Alert";
import {Button} from "react-bootstrap";

/**
 * Function is responsible for dismissing a DismissibleAlert.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const hideAlert = (component) => component.setState({alert: null});

/**
 * Function is responsible for rendering a single DismissibleAlert
 * based on the provided properties.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const DismissibleAlert = (props) =>
    <Alert show={true} variant={props.variant}>
        <Alert.Heading>{props.heading}</Alert.Heading>
        <hr/>
        {Array.isArray(props.body) ? props.body.map(m => <p>{m}</p>) : props.body}
        <div className="d-flex justify-content-end">
            <Button onClick={props.handleHide} variant={"outline-" + props.variant}>
                Dismiss
            </Button>
        </div>
    </Alert>;

/**
 * Function is responsible for rendering a single failure DismissibleAlert.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const FailureAlert = (props) =>
    <DismissibleAlert
        variant="danger"
        heading="Oops! Something went wrong!"
        body={props["messages"]}
        handleHide={() => hideAlert(props["component"])}
    />;

/**
 * Function is responsible for rendering a single success DismissibleAlert.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export const SuccessAlert = (props) =>
    <DismissibleAlert
        variant="success"
        heading="Success!"
        body={props["messages"]}
        handleHide={() => hideAlert(props["component"])}
    />;