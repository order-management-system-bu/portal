import React from "react";
import Nav from "react-bootstrap/Nav";
import Card from "react-bootstrap/Card";

/**
 * Function is responsible for rendering the customer control navigation
 * wrapper.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const CustomerControlNavbarWrapper = (props) =>
    <Card className="width-handler margin-top-l">
        <Card.Header>
            <Nav variant="tabs" defaultActiveKey={props["activeKey"]}>
                <Nav.Item>
                    <Nav.Link eventKey="accountView" href="/customer/update">
                        Your Account
                    </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="ordersView" href="/customer/orders">
                        Your Orders
                    </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="cardsView" href="/customer/cards">
                        Your Saved Cards
                    </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="addressesView" href="/customer/addresses">
                        Your Delivery Addresses
                    </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="notificationsView" href="/customer/preferences">
                        Notification Preferences
                    </Nav.Link>
                </Nav.Item>
            </Nav>
        </Card.Header>
        <Card.Body>
            {props["pageContent"]}
        </Card.Body>
    </Card>;

export default CustomerControlNavbarWrapper;