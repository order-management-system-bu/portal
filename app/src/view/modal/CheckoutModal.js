import React from "react";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import {authGet, requestGet, requestPost, RmwEndpoints} from "../../utility/request-handler";
import {
    cleanupFormMessages,
    constructErrorMessage,
    constructSuccessMessage,
    handleChange
} from "../../utility/form-functions";
import Card from "react-bootstrap/Card";
import {beutifyString} from "../../utility/string-functions";
import {FailureAlert} from "../component/DismissibleAlert";

export default class CheckoutModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,

            customer: null,
            deliveryAddress: null,
            paymentDetail: null,

            customerName: "",
            customerEmail: "",
            customerContactNumber: "",

            street: "",
            city: "",
            county: "",
            postcode1: "",
            postcode2: "",

            cardType: "VISA",
            nameOnCard: "",
            cardNumber: "",
            expiryMonth: 2019,
            expiryYear: 1,

            discountCode: "",
            appliedDiscountCode: null,

            addressManualOverride: false,
            paymentDetailManualOverride: false
        }
    }

    componentDidMount() {
        const getActiveAddress = (customer) => authGet(
            RmwEndpoints.Urm.VIEW_ACTIVE_ADDRESS(customer["id"]),
            (response) => this.setState({
                customer: customer,
                deliveryAddress: response.data,
                street: response.data["street"],
                city: response.data["city"],
                county: response.data["county"],
                postcode1: response.data["postcode1"],
                postcode2: response.data["postcode2"],
            })
        );

        const getActivePaymentDetail = (customer) => authGet(
            RmwEndpoints.Urm.VIEW_ACTIVE_PAYMENT_DETAIL(customer["id"]),
            (response) => this.setState({
                customer: customer,
                paymentDetail: response.data,
                cardType: response.data["cardType"],
                nameOnCard: response.data["nameOnCard"],
                cardNumber: response.data["cardNumber"],
                cardExpiry: response.data["cardExpiry"]
            })
        );

        if (this.state.customer === null) {
            authGet(
                RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN,
                (response) => {
                    getActiveAddress(response.data);
                    getActivePaymentDetail(response.data);
                    this.setState({
                        customerName: response.data["forename"] + " " + response.date["surname"],
                        customerEmail: response.date["username"],
                        customerContactNumber: response.date["customerContactNumber"]
                    })
                }
            )
        } else {
            getActiveAddress(this.state.customer);
            getActivePaymentDetail(this.state.customer);
        }
    }

    handleFormSubmit = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        const subtotal = this.props.calculateSubtotal();
        const deliveryLimit = parseFloat(this.props["minimumForDelivery"]);
        if (subtotal < deliveryLimit) {
            this.setState({
                alert: <FailureAlert
                    messages={"Minimum spend for delivery has not been reached. Please spend another: £" + (deliveryLimit - subtotal).toFixed(2)}
                    component={this}
                />
            })
        } else {
            requestPost(
                RmwEndpoints.Om.NEW_ORDER,
                {
                    customerId: this.state.customer ? this.state.customer["id"] : null,
                    customerName: this.state.customerName,
                    customerEmail: this.state.customerEmail,
                    customerContactNumber: this.state.customerContactNumber,
                    orderOptions: this.props["basket"].map(basketItem => JSON.parse(JSON.stringify({
                        menuItemId: basketItem["menuItemId"],
                        selectedOptionIds: basketItem["selectedOptions"].map(option => option["id"])
                    }))),
                    cardType: this.state.cardType,
                    cardNumber: this.state.cardNumber,
                    cardExpiry: new Date(this.state.expiryYear, this.state.expiryMonth - 1, 1),
                    nameOnCard: this.state.nameOnCard,
                    street: this.state.street,
                    city: this.state.city,
                    county: this.state.county,
                    postcode1: this.state.postcode1,
                    postcode2: this.state.postcode2,
                    discountCode: this.state.discountCode
                },
                (response) => {
                    this.props.onHide();
                    this.props.redirectToOrderSummary(response.data);
                },
                event.target
            );
        }
    };

    handleApplyDiscountCode = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        requestGet(
            RmwEndpoints.Om.VIEW_DISCOUNT_CODE(this.state.discountCode),
            (response) => {
                if (!response.data["enabled"]) {
                    document.getElementById("discountCode").parentNode.appendChild(constructErrorMessage(
                        "Discount code is no longer active."
                    ));
                } else {
                    const basketTotal = this.props.calculateSubtotal();
                    const minimumSpend = response.data["spendEligibility"] ? parseFloat(response.data["spendEligibility"]) : null;
                    if (minimumSpend && basketTotal < minimumSpend) {
                        document.getElementById("discountCode").parentNode.appendChild(constructErrorMessage(
                            "Minimum spend for this code has not been reached. Please spend another: £" + (minimumSpend - basketTotal).toFixed(2)
                        ));
                    } else {
                        document.getElementById("discountCode").parentNode.appendChild(constructSuccessMessage(
                            "Discount code successfully applied!"
                        ));
                        this.setState({
                            appliedDiscountCode: response.data
                        })
                    }
                }
            }, () => document.getElementById("discountCode").parentNode.appendChild(constructErrorMessage(
                "The provided discount code was not valid."
            ))
        )
    };

    render() {
        const maybeSubtotal = this.props.calculateSubtotal();
        let subtotal = "0.00";
        let discount = "0.00";
        if (maybeSubtotal) {
            if (this.state.appliedDiscountCode !== null)
                if (this.state.appliedDiscountCode["discountType"] === "PERCENTAGE") {
                    subtotal = maybeSubtotal - (maybeSubtotal * (this.state.appliedDiscountCode["discount"] / 100)).toFixed(2);
                    discount = maybeSubtotal * (this.state.appliedDiscountCode["discount"] / 100).toFixed(2);
                } else {
                    subtotal = maybeSubtotal - this.state.appliedDiscountCode["discount"];
                    discount = this.state.appliedDiscountCode["discount"].toFixed(2);
                }
            else subtotal = maybeSubtotal.toFixed(2);
        }
        const deliveryFee = this.props["deliveryFee"] ? this.props["deliveryFee"].toFixed(2) : "0.00";

        return (
            <Modal {...this.props} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Checkout</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.state.alert}
                    <Table responsive="sm">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props["basket"].map(basketItem =>
                            <tr>
                                <td>
                                    <h5>{basketItem["title"]}</h5>
                                    {basketItem["selectedOptions"] === undefined || basketItem["selectedOptions"].length === 0 ? null :
                                        basketItem["selectedOptions"].map(option =>
                                            <p className="padding-left-s margin-none font-grey font-s">
                                                {option["title"]}
                                            </p>
                                        )
                                    }
                                </td>
                                <td>
                                    <h5>{"£" + basketItem["price"]}</h5>
                                    {basketItem["selectedOptions"] === undefined || basketItem["selectedOptions"].length === 0 ? null :
                                        basketItem["selectedOptions"].map(option =>
                                            <p className="padding-left-s margin-none font-grey font-s">
                                                {"£" + option["price"].toFixed(2)}
                                            </p>
                                        )
                                    }
                                </td>
                                <td>
                                    <h5>{"£" + this.props.calculateBasketItemPrice(basketItem).toFixed(2)}</h5>
                                </td>
                            </tr>
                        )}
                        </tbody>
                    </Table>
                    <hr/>
                    <br/>
                    <Container>
                        <Form onSubmit={e => this.handleFormSubmit(e)}>
                            {this.state.customer !== null ? null :
                                <Container>
                                    <Row>
                                        <Col xs={6}>
                                            <h3 className="font-black font-l">
                                                <b>Customer Information</b>
                                            </h3>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6}>
                                            {["customerName", "customerEmail", "customerContactNumber"].map(name =>
                                                <Row className="margin-top-s">
                                                    <Col>
                                                        <Form.Group onChange={e => handleChange(e, this)}>
                                                            <Form.Label className="font-black font-s">
                                                                {beutifyString(name.replace("customer", ""))}
                                                            </Form.Label>
                                                            <Form.Control id={name} size="sm" type="text"
                                                                          maxlength="30"/>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                            )}
                                        </Col>
                                    </Row>
                                    <br/>
                                </Container>
                            }
                            <Row>
                                <Col xs={6}>
                                    <h3 className="font-black font-l">
                                        <b>Delivery Address</b>
                                    </h3>
                                </Col>
                                <Col xs={6}>
                                    <h3 className="font-black font-l">
                                        <b>Payment Details</b>
                                    </h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={6}>
                                    {this.state.deliveryAddress !== null && !this.state.addressManualOverride ?
                                        <Card>
                                            <Card.Body>
                                                {[this.state.deliveryAddress["street"], this.state.deliveryAddress["city"], this.state.deliveryAddress["postcode1"] + " " + this.state.deliveryAddress["postcode2"]].map(element =>
                                                    <Card.Text className="margin-xs">
                                                        {element}
                                                    </Card.Text>
                                                )}
                                                <Button type="button" variant="primary" size="sm" block
                                                        onClick={() => this.setState({addressManualOverride: true})}>
                                                    Enter Delivery Address
                                                </Button>
                                            </Card.Body>
                                        </Card> : <Container>
                                            {["street", "city", "county"].map(name =>
                                                <Row className="margin-top-s">
                                                    <Col>
                                                        <Form.Group onChange={e => handleChange(e, this)}>
                                                            <Form.Label className="font-black font-s">
                                                                {beutifyString(name)}
                                                            </Form.Label>
                                                            <Form.Control id={name} size="sm" type="text"
                                                                          maxlength="30"/>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                            )}
                                            <Row className="margin-top-s">
                                                {["postcode1", "postcode2"].map(name =>
                                                    <Col xs={6}>
                                                        <Form.Group onChange={e => handleChange(e, this)}>
                                                            <Form.Label className="font-black font-s">
                                                                {beutifyString(name)}
                                                            </Form.Label>
                                                            <Form.Control id={name} size="sm" type="text"
                                                                          maxlength="4"/>
                                                        </Form.Group>
                                                    </Col>
                                                )}
                                            </Row>
                                        </Container>
                                    }
                                </Col>
                                <Col xs={6}>
                                    {this.state.paymentDetail !== null && !this.state.paymentDetailManualOverride ?
                                        <Card>
                                            <Card.Body>
                                                {[["cardType", this.state.paymentDetail["cardType"]], ["nameOnCard", this.state.paymentDetail["nameOnCard"]], ["cardNumber", this.state.paymentDetail["cardNumber"]]].map(tuple =>
                                                    <Card.Text className="margin-xs">
                                                        <b>{beutifyString(tuple[0])}: </b> {tuple[1]}
                                                    </Card.Text>
                                                )}
                                                <Card.Text className="margin-xs">
                                                    <b>Card Expiry: </b>
                                                    {new Date(this.state.paymentDetail["cardExpiry"]).getMonth() + "/" + new Date(this.state.paymentDetail["cardExpiry"]).getFullYear()}
                                                </Card.Text>
                                                <Button type="button" variant="primary" size="sm" block
                                                        onClick={() => this.setState({paymentDetailManualOverride: true})}>
                                                    Enter Payment Details
                                                </Button>
                                            </Card.Body>
                                        </Card> : <Container>
                                            <Row className="margin-top-s">
                                                <Col>
                                                    <Form.Group controlId="cardType"
                                                                onChange={e => handleChange(e, this)}>
                                                        <Form.Label className="font-black font-s">Card Type</Form.Label>
                                                        <Form.Control as="select" size="sm">
                                                            <option>VISA</option>
                                                            <option>Mastercard</option>
                                                            <option>American Express</option>
                                                        </Form.Control>
                                                    </Form.Group>
                                                </Col>
                                            </Row>
                                            {["nameOnCard", "cardNumber"].map(name =>
                                                <Row className="margin-top-s">
                                                    <Col>
                                                        <Form.Group onChange={e => handleChange(e, this)}>
                                                            <Form.Label className="font-black font-s">
                                                                {beutifyString(name)}
                                                            </Form.Label>
                                                            <Form.Control id={name} size="sm" type="text"
                                                                          maxlength="30"/>
                                                        </Form.Group>
                                                    </Col>
                                                </Row>
                                            )}
                                            <Row className="margin-top-s">
                                                <Col>
                                                    <Row>
                                                        <Col xs={6}>
                                                            <Form.Group controlId="expiryMonth"
                                                                        onChange={e => handleChange(e, this)}>
                                                                <Form.Label className="font-black font-s">Expiry
                                                                    Month</Form.Label>
                                                                <Form.Control as="select" size="sm">
                                                                    {Array(12 - 1 + 1).fill().map((_, idx) => 1 + idx).map(month =>
                                                                        <option>{month}</option>
                                                                    )}
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                        <Col xs={6}>
                                                            <Form.Group controlId="expiryYear"
                                                                        onChange={e => handleChange(e, this)}>
                                                                <Form.Label className="font-black font-s">Expiry
                                                                    Year</Form.Label>
                                                                <Form.Control as="select" size="sm">
                                                                    {Array(2099 - 2019 + 1).fill().map((_, idx) => 2019 + idx).map(year =>
                                                                        <option>{year}</option>
                                                                    )}
                                                                </Form.Control>
                                                            </Form.Group>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </Container>
                                    }
                                </Col>
                            </Row>
                            <hr/>
                            <br/>
                            <Row>
                                <Col xs={4}>
                                    <Form onSubmit={e => this.handleApplyDiscountCode(e)}>
                                        <Form.Group onChange={e => handleChange(e, this)}>
                                            <Form.Label className="font-black font-s">
                                                Discount Code
                                            </Form.Label>
                                            <Form.Control id="discountCode" size="sm" type="text" maxLength="20"/>
                                        </Form.Group>
                                        <Button type="submit" variant="primary" size="sm" block>
                                            Apply
                                        </Button>
                                    </Form>
                                </Col>
                                <Col xs={4}/>
                                <Col xs={4}>
                                    <Row>
                                        <Col xs={7}><b>Subtotal: </b></Col>
                                        <Col xs={5}>{"£" + parseFloat(subtotal).toFixed(2)}</Col>
                                    </Row>
                                    <Row>
                                        <Col xs={7}><b>Discount: </b></Col>
                                        <Col xs={5}>{"-£" + parseFloat(discount).toFixed(2)}</Col>
                                    </Row>
                                    <Row>
                                        <Col xs={7}><b>Delivery: </b></Col>
                                        <Col xs={5}>{"£" + deliveryFee}</Col>
                                    </Row>
                                    <Row>
                                        <Col xs={7}><b>Total: </b></Col>
                                        <Col xs={5}>
                                            {"£" + (parseFloat(subtotal) + parseFloat(deliveryFee)).toFixed(2)}
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col>
                                            <Button type="submit" variant="success" size="md" block>
                                                Checkout
                                            </Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Form>
                    </Container>
                </Modal.Body>
            </Modal>
        )
    }
}