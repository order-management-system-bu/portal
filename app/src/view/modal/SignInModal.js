import React from "react";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {cleanupFormMessages, handleChange} from "../../utility/form-functions";
import {authPost, RmwEndpoints} from "../../utility/request-handler";

/**
 * Class is responsible for rendering the customer sign in modal
 * and handling customer authentication.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class SignInModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,

            username: "",
            password: ""
        }
    }

    handleFormSubmit = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        authPost(
            RmwEndpoints.Urm.AUTHENTICATE,
            {
                username: this.state.username,
                password: this.state.password
            },
            (response) => {
                localStorage.setItem("oms.authentication.key", JSON.stringify({
                    token: response.data,
                    time: new Date().getTime()
                }));
                this.props.fetchCustomerByAuthToken();
                this.props.onHide();
                window.location.reload();
            },
            event.target)
    };

    render() {
        return (
            <Modal {...this.props} size="md" centered>
                <Modal.Body>
                    {this.state.alert}
                    <Modal.Header className="font-black font-xl">
                        Sign In
                    </Modal.Header>
                    <br/>
                    <Form onSubmit={e => this.handleFormSubmit(e)}>
                        <Form.Group onChange={e => handleChange(e, this)}>
                            <Form.Label className="font-black font-s"><b>Email Address</b></Form.Label>
                            <Form.Control id="username" size="sm" type="email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group onChange={e => handleChange(e, this)}>
                            <Form.Label className="font-black font-s"><b>Password</b></Form.Label>
                            <Form.Control id="password" size="sm" type="password"/>
                        </Form.Group>
                        <Button type="submit" block size="sm">Sign In</Button>
                    </Form>
                    <br/>
                    <p>Don't have an account? Sign up <a href="#" onClick={this.props["showSignUpModal"]}>here</a>.
                    </p>
                </Modal.Body>
            </Modal>
        )
    }
}