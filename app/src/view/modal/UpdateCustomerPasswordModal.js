import React from "react"
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import {cleanupFormMessages, handleChange} from "../../utility/form-functions";
import {beutifyString} from "../../utility/string-functions";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {authPut, RmwEndpoints} from "../../utility/request-handler";
import {SuccessAlert} from "../component/DismissibleAlert";

/**
 * Class is responsible for rendering the UpdateCustomerPassword modal.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class UpdateCustomerPasswordModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,

            password: "",
            confirmPassword: ""
        }
    }

    handleFormSubmit(event) {
        event.preventDefault();
        cleanupFormMessages();
        authPut(
            RmwEndpoints.Urm.UPDATE_CUSTOMER(this.props["customerId"]),
            {
                password: this.state.password
            },
            () => this.setState({
                alert: <SuccessAlert messages={"Successfully updated your password."} component={this}/>
            }),
            event.target
        );
    }

    render() {
        return (
            <Modal {...this.props} size="lg">
                <Modal.Header closeButton>
                    Update Password
                </Modal.Header>
                <Modal.Body>
                    {this.state.alert}
                    <Form onSubmit={e => this.handleFormSubmit(e)}>
                        {["password", "confirmPassword"].map(name =>
                            <Form.Group onChange={e => handleChange(e, this)}>
                                <Form.Label className="font-black font-s">
                                    <b>{beutifyString(name)}</b>
                                </Form.Label>
                                <Form.Control id={name} size="sm" type="password"/>
                            </Form.Group>
                        )}
                        <Row>
                            <Col xs={6}>
                                <Button variant="outline-primary" block onClick={this.props["onHide"]}>
                                    Cancel
                                </Button>
                            </Col>
                            <Col xs={6}>
                                <Button type="submit" variant="primary" size="md" block>
                                    Update Password
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}