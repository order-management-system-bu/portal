import React from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {authDelete, RmwEndpoints} from "../../utility/request-handler";
import {FailureAlert} from "../component/DismissibleAlert";

/**
 * Class is responsible for rendering the remove
 * account modal.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export class DeleteCustomerModal extends React.Component {

    deleteAccount = (event) => {
        event.preventDefault();
        authDelete(
            RmwEndpoints.Urm.REMOVE_CUSTOMER(this.props["customerId"]),
            () => {
                localStorage.removeItem("oms.authentication.key");
                window.location.reload();
            },
            (error) => {
                const defaultError = "An unexpected error occurred when deleting the customer.";
                this.setState({
                    alert: <FailureAlert
                        messages={error.response.data ? error.response.data.map(vf => vf["fault"]) : defaultError}
                        component={this}
                    />
                })
            }
        )
    };

    render() {
        return (
            <Modal {...this.props} size="md">
                <Modal.Header closeButton>
                    <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col>
                            Warning! Deleting your account cannot be undone and
                            data cannot be recovered. Are you sure you want to
                            continue?
                        </Col>
                    </Row>
                    <hr/>
                    <Row>
                        <Col xs={6}>
                            <Button variant="primary" block onClick={this.props["onHide"]}>
                                Cancel
                            </Button>
                        </Col>
                        <Col xs={6}>
                            <Button variant="danger" block onClick={e => this.deleteAccount(e)}>
                                Confirm
                            </Button>
                        </Col>
                    </Row>
                </Modal.Body>
            </Modal>
        )
    }
}