import React from "react";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {authGet, RmwEndpoints} from "../../utility/request-handler";

/**
 * Class is responsible for rendering the add to basket modal.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class AddToBasketModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menuItemCount: 1,
            additionalOptions: [],

            selectedMenuOptions: [],
            subTotal: ""
        }
    }

    componentDidMount() {
        if (this.props["menuItem"]["additionalOptionGroupIds"] !== undefined || this.props["menuItem"]["additionalOptionGroupIds"].length > 0) {
            authGet(
                RmwEndpoints.Om.LIST_MENU_OPTION_GROUPS(this.props["menuItem"]["additionalOptionGroupIds"]),
                (response) => {
                    const additionalOptionIds = response.data.map(mio => mio["menuItemOptions"]);
                    if (this.props["menuItem"]["additionalOptionIds"] !== undefined || this.props["menuItem"]["additionalOptionIds"].length > 0)
                        additionalOptionIds.concat(this.props["menuItem"]["additionalOptionIds"]);
                    authGet(
                        RmwEndpoints.Om.LIST_MENU_OPTIONS(additionalOptionIds),
                        (response) => this.setState({
                            additionalOptions: this.state.additionalOptions.concat(response.data)
                        })
                    )
                }
            )
        } else {
            authGet(
                RmwEndpoints.Om.LIST_MENU_OPTIONS(this.props["menuItem"]["additionalOptionIds"]),
                (response) => this.setState({
                    additionalOptions: this.state.additionalOptions.concat(response.data)
                })
            )
        }

        this.setState({
            subTotal: this.props["menuItem"]["price"]
        });
    }

    handleChange = (event, title, price) => {
        const optionPrice = this.state.additionalOptions.filter(mi => mi["id"] === event.target.id)[0]["price"];
        if (event.target.checked) {
            this.setState({
                subTotal: this.state.subTotal + optionPrice,
                selectedMenuOptions: this.state.selectedMenuOptions.concat({
                    id: event.target.id,
                    title: title,
                    price: price
                })
            });
        } else {
            this.setState({
                subTotal: this.state.subTotal - optionPrice,
                selectedMenuOptions: this.state.selectedMenuOptions.filter(mo => mo["id"] !== event.target.id)
            })
        }
    };

    render() {
        return (
            <Modal {...this.props} size="md" centered>
                <Modal.Body>
                    <Modal.Title className="font-black font-xl">
                        {this.props["menuItem"]["title"]}
                    </Modal.Title>
                    <Modal.Title className="font-grey font-s">
                        {this.props["menuItem"]["description"]}
                    </Modal.Title>
                    <hr/>
                    <Modal.Title className="font-black font-xl">
                        Additional Options
                    </Modal.Title>
                    <br/>
                    <Form>
                        {this.state.additionalOptions.map(menuOption =>
                            <Row>
                                <Col xs={3}>
                                    <label className="checkbox-container">
                                        <input id={menuOption["id"]} type="checkbox"
                                               checked={this.state.receiveNewsAndOffersSms}
                                               onClick={e => this.handleChange(e, menuOption["title"], menuOption["price"])}/>
                                        <span className="checkbox-checkmark"/>
                                    </label>
                                </Col>
                                <Col xs={6}>
                                    <p>{menuOption["title"]}</p>
                                </Col>
                                <Col className="text-center" xs={3}>
                                    <p>£{menuOption["price"].toFixed(2)}</p>
                                </Col>
                            </Row>
                        )}
                        <br/>
                        <Row>
                            <Col xs={4}>
                                <Button variant="outline-primary" block onClick={this.props["onHide"]}>
                                    Cancel
                                </Button>
                            </Col>
                            <Col xs={8}>
                                <Button
                                    variant="primary"
                                    block
                                    onClick={() => {
                                        this.props.addToBasket({
                                            menuItemId: this.props["menuItem"]["id"],
                                            selectedOptions: this.state.selectedMenuOptions,
                                            title: this.props["menuItem"]["title"],
                                            price: this.props["menuItem"]["price"]
                                        });
                                        this.props.onHide()
                                    }}
                                >
                                    Add to basket: £{parseFloat(this.state.subTotal).toFixed(2)}
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}