import React from "react";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {SuccessAlert} from "../component/DismissibleAlert";
import {cleanupFormMessages, handleChange} from "../../utility/form-functions";
import {requestPost, RmwEndpoints} from "../../utility/request-handler";
import {beutifyString} from "../../utility/string-functions";

/**
 * Class is responsible for rendering the customer sign up modal
 * and handling customer sing up functionality.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class SignUpModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,

            username: "",
            password: "",
            confirmPassword: "",
            forename: "",
            surname: "",
            contactNumber: "",
            receiveAccountNotifications: true,
            receiveOrderUpdates: true,
            receiveNewsAndOffersEmail: true,
            receiveNewsAndOffersSms: true
        }
    }

    handleFormSubmit = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        requestPost(
            RmwEndpoints.Urm.NEW_CUSTOMER,
            JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                forename: this.state.forename,
                surname: this.state.surname,
                contactNumber: this.state.contactNumber,
                receiveAccountNotifications: this.state.receiveAccountNotifications,
                receiveOrderUpdates: this.state.receiveOrderUpdates,
                receiveNewsAndOffersEmail: this.state.receiveNewsAndOffersEmail,
                receiveNewsAndOffersSms: this.state.receiveNewsAndOffersSms
            }),
            () => {
                const successString = "Successfully created a new account! An activation email has been sent to " +
                    this.state.username + ". Please check your junk folder if you cannot find the email.";
                this.setState({alert: <SuccessAlert messages={successString} component={this}/>})
            },
            event.target
        );
    };

    render() {
        const handleCheckboxChange = (event) => this.setState({[event.target.id]: event.target.checked});

        return (
            <Modal {...this.props} size="md" centered>
                <Modal.Body>
                    {this.state.alert}
                    <Modal.Header className="font-black font-xl">
                        Create Account
                    </Modal.Header>
                    <Form onSubmit={e => this.handleFormSubmit(e)}>
                        <Form.Group onChange={e => handleChange(e, this)}>
                            <Form.Label className="font-black font-s"><b>Email address</b></Form.Label>
                            <Form.Control id="username" size="sm" type="email"/>
                        </Form.Group>
                        <Row>
                            {["password", "confirmPassword"].map(name =>
                                <Col>
                                    <Form.Group onChange={e => handleChange(e, this)}>
                                        <Form.Label
                                            className="font-black font-s"><b>{beutifyString(name)}</b></Form.Label>
                                        <Form.Control id={name} size="sm" type="password"/>
                                    </Form.Group>
                                </Col>
                            )}
                        </Row>
                        <Form.Group onChange={e => handleChange(e, this)}>
                            <Form.Label className="font-black font-s"><b>Mobile number</b></Form.Label>
                            <Form.Control id="contactNumber" size="sm" type="text" maxlength="11"/>
                        </Form.Group>
                        <Row>
                            {["forename", "surname"].map(name =>
                                <Col>
                                    <Form.Group onChange={e => handleChange(e, this)}>
                                        <Form.Label className="font-black font-s"><b>Forename</b></Form.Label>
                                        <Form.Control id={name} size="sm" type="text" maxlength="30"/>
                                    </Form.Group>
                                </Col>
                            )}
                        </Row>
                        <Row>
                            <Col>
                                <label className="checkbox-container">
                                    Receive Account Notifications
                                    <input id="receiveAccountNotifications" type="checkbox"
                                           checked={this.state.receiveAccountNotifications}
                                           onClick={e => handleCheckboxChange(e)}/>
                                    <span className="checkbox-checkmark"/>
                                </label>
                            </Col>
                            <Col>
                                <label className="checkbox-container">
                                    Receive Order Notifications
                                    <input id="receiveOrderUpdates" type="checkbox"
                                           checked={this.state.receiveOrderUpdates}
                                           onClick={e => handleCheckboxChange(e)}/>
                                    <span className="checkbox-checkmark"/>
                                </label>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col>
                                <label className="checkbox-container">
                                    Receive News and Discounts (Email)
                                    <input id="receiveNewsAndOffersEmail" type="checkbox"
                                           checked={this.state.receiveNewsAndOffersEmail}
                                           onClick={e => handleCheckboxChange(e)}/>
                                    <span className="checkbox-checkmark"/>
                                </label>
                            </Col>
                            <Col>
                                <label className="checkbox-container">
                                    Receive News and Discounts (SMS)
                                    <input id="receiveNewsAndOffersSms" type="checkbox"
                                           checked={this.state.receiveNewsAndOffersSms}
                                           onClick={e => handleCheckboxChange(e)}/>
                                    <span className="checkbox-checkmark"/>
                                </label>
                            </Col>
                        </Row>
                        <br/>
                        <Button block size="sm" type="submit">Sign Up</Button>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}