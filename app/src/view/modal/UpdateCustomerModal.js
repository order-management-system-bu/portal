import React from "react";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {cleanupFormMessages, handleChange} from "../../utility/form-functions";
import {beutifyString} from "../../utility/string-functions";
import {authPut, RmwEndpoints} from "../../utility/request-handler";

/**
 * Class is responsible for rendering the UpdateCustomer modal.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class UpdateCustomerModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alert: null,

            customerId: "",
            forename: "",
            surname: "",
            contactNumber: ""
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            customerId: nextProps["customerId"],
            forename: nextProps["forename"],
            surname: nextProps["surname"],
            contactNumber: nextProps["contactNumber"]
        })
    }

    handleFormSubmit(event) {
        event.preventDefault();
        cleanupFormMessages();
        authPut(RmwEndpoints.Urm.UPDATE_CUSTOMER(this.state.customerId),
            {
                "forename": this.state.forename,
                "surname": this.state.surname,
                "contactNumber": this.state.contactNumber
            },
            () => window.location.reload(),
            event.target
        );
    }

    render() {
        return (
            <Modal {...this.props} size="lg">
                <Modal.Header closeButton>
                    Update Account Details
                </Modal.Header>
                <Modal.Body>
                    {this.state.alert}
                    <Form onSubmit={e => this.handleFormSubmit(e)}>
                        {["forename", "surname"].map(name =>
                            <Form.Group onChange={e => handleChange(e, this)}>
                                <Form.Label className="font-black font-s">
                                    <b>{beutifyString(name)}</b>
                                </Form.Label>
                                <Form.Control id={name} size="sm" type="text" defaultValue={this.props[name]}
                                              maxlength="30"/>
                            </Form.Group>
                        )}
                        <Form.Group onChange={e => handleChange(e, this)}>
                            <Form.Label className="font-black font-s">
                                <b>Contact Number</b>
                            </Form.Label>
                            <Form.Control id="contactNumber" size="sm" type="text"
                                          defaultValue={this.props.contactNumber} maxlength="11"/>
                        </Form.Group>
                        <Row>
                            <Col xs={6}>
                                <Button variant="outline-primary" size="md" block onClick={this.props["onHide"]}>
                                    Cancel
                                </Button>
                            </Col>
                            <Col xs={6}>
                                <Button type="submit" variant="primary" size="md" block>
                                    Save Changes
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}