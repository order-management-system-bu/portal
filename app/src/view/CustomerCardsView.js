import React from "react"
import CustomerControlNavbarWrapper from "./component/CustomerControlNavbarWrapper";
import {authDelete, authGet, authPost, authPut, RmwEndpoints} from "../utility/request-handler";
import {cleanupFormMessages, handleChange} from "../utility/form-functions";
import {FailureAlert, SuccessAlert} from "./component/DismissibleAlert";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {beutifyString} from "../utility/string-functions";

/**
 * Class is responsible for rendering the CustomerCardsView.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class CustomerCardsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: null,
            paymentDetails: [],

            customerId: "",
            cardType: "VISA",
            nameOnCard: "",
            cardNumber: "",

            expiryMonth: 2019,
            expiryYear: 1
        }
    }

    componentDidMount() {
        this.fetchPaymentDetails();
    }

    fetchPaymentDetails() {
        if (this.state.customerId === "") {
            authGet(RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN, (customerResponse) => {
                    this.setState({
                        customerId: customerResponse.data["id"]
                    });
                    authGet(RmwEndpoints.Urm.LIST_PAYMENT_DETAIL(customerResponse.data["id"]), (paymentDetailResponse) => this.setState({
                        paymentDetails: paymentDetailResponse.data
                    }))
                }
            )
        } else {
            authGet(RmwEndpoints.Urm.LIST_PAYMENT_DETAIL(this.state.customerId), (paymentDetailResponse) => {
                this.setState({
                    paymentDetails: paymentDetailResponse.data
                })
            })
        }
    }

    handleAssignDefaultPaymentDetail = (event, paymentDetailId) => {
        event.preventDefault();
        cleanupFormMessages();
        authPut(
            RmwEndpoints.Urm.ASSIGN_ACTIVE_PAYMENT_DETAIL(paymentDetailId),
            null,
            () => {
                this.setState({
                    alert: <SuccessAlert messages="Successfully changed your default payment details."
                                         component={this}/>
                });
                this.fetchPaymentDetails();
            },
            event.target
        )
    };

    handleCreatePaymentDetail = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        authPost(
            RmwEndpoints.Urm.NEW_PAYMENT_DETAIL,
            {
                customerId: this.state.customerId,
                cardType: this.state.cardType,
                nameOnCard: this.state.nameOnCard,
                cardNumber: this.state.cardNumber,
                cardExpiry: new Date(this.state.expiryYear, this.state.expiryMonth - 1, 1)
            }, () => {
                this.setState({
                    alert: <SuccessAlert messages="Successfully stored your new default payment details."
                                         component={this}/>
                });
                this.fetchPaymentDetails();
            },
            event.target
        )
    };

    handleDeletePaymentDetail = (event, paymentDetailId) => {
        event.preventDefault();
        authDelete(
            RmwEndpoints.Urm.DELETE_PAYMENT_DETAIL(paymentDetailId),
            () => {
                this.setState({
                    alert: <SuccessAlert messages="Successfully removed the payment detail." component={this}/>
                });
                this.fetchPaymentDetails();
            },
            () => {
                this.setState({
                    alert: <FailureAlert
                        messages="An unexpected error occurred when removing the payment detail."
                        component={this}
                    />
                });
                this.fetchPaymentDetails();
            }
        )
    };

    render() {
        return (
            <CustomerControlNavbarWrapper
                activeKey="cardsView"
                pageContent={
                    <div>
                        {this.state.alert}
                        <Row>
                            <Col>
                                <h4 className="font-black font-xl text-center">
                                    Your Payment Details
                                </h4>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            {this.state.paymentDetails.map(paymentDetail =>
                                <Col className="padding-m" xs={4}>
                                    <Card>
                                        {!paymentDetail["default"] ? null :
                                            <Card.Header>
                                                <Card.Title className="margin-none">Default Payment Details</Card.Title>
                                            </Card.Header>
                                        }
                                        <Card.Body>
                                            {[["cardType", paymentDetail["cardType"]], ["nameOnCard", paymentDetail["nameOnCard"]], ["cardNumber", paymentDetail["cardNumber"]]].map(tuple =>
                                                <Card.Text className="margin-xs">
                                                    <b>{beutifyString(tuple[0])}: </b> {tuple[1]}
                                                </Card.Text>
                                            )}
                                            <Card.Text className="margin-xs">
                                                <b>Card Expiry: </b>
                                                {new Date(paymentDetail["cardExpiry"]).getMonth() + "/" + new Date(paymentDetail["cardExpiry"]).getFullYear()}
                                            </Card.Text>
                                            {paymentDetail["default"] ? null :
                                                <Row>
                                                    <Col xs={6}>
                                                        <Form
                                                            onSubmit={e => this.handleDeletePaymentDetail(e, paymentDetail["id"])}>
                                                            <Button type="submit" variant="danger" size="sm" block>
                                                                Remove
                                                            </Button>
                                                        </Form>
                                                    </Col>
                                                    <Col xs={6}>
                                                        <Form
                                                            onSubmit={e => this.handleAssignDefaultPaymentDetail(e, paymentDetail["id"])}>
                                                            <Button type="submit" size="sm" block>
                                                                Assign Default
                                                            </Button>
                                                        </Form>
                                                    </Col>
                                                </Row>
                                            }
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )}
                        </Row>
                        <hr/>
                        <div>
                            <Row>
                                <Col>
                                    <h4 className="font-black font-l">
                                        Add new payment details:
                                    </h4>
                                </Col>
                            </Row>
                            <Form onSubmit={e => this.handleCreatePaymentDetail(e)}>
                                <Row className="margin-top-s">
                                    <Col xs={4}>
                                        <Form.Group controlId="cardType" onChange={e => handleChange(e, this)}>
                                            <Form.Label className="font-black font-s">Card Type</Form.Label>
                                            <Form.Control as="select" size="sm">
                                                <option>VISA</option>
                                                <option>Mastercard</option>
                                                <option>American Express</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                {["nameOnCard", "cardNumber"].map(name =>
                                    <Row className="margin-top-s">
                                        <Col xs={4}>
                                            <Form.Group onChange={e => handleChange(e, this)}>
                                                <Form.Label className="font-black font-s">
                                                    {beutifyString(name)}
                                                </Form.Label>
                                                <Form.Control id={name} size="sm" type="text" maxlength="30"/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                )}
                                <Row>
                                    <Col xs={4}>
                                        <Row>
                                            <Col xs={6}>
                                                <Form.Group controlId="expiryMonth"
                                                            onChange={e => handleChange(e, this)}>
                                                    <Form.Label className="font-black font-s">Expiry Month</Form.Label>
                                                    <Form.Control as="select" size="sm">
                                                        {Array(12 - 1 + 1).fill().map((_, idx) => 1 + idx).map(month =>
                                                            <option>{month}</option>
                                                        )}
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                            <Col xs={6}>
                                                <Form.Group controlId="expiryYear"
                                                            onChange={e => handleChange(e, this)}>
                                                    <Form.Label className="font-black font-s">Expiry Year</Form.Label>
                                                    <Form.Control as="select" size="sm">
                                                        {Array(2099 - 2019 + 1).fill().map((_, idx) => 2019 + idx).map(year =>
                                                            <option>{year}</option>
                                                        )}
                                                    </Form.Control>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row className="margin-top-s">
                                    <Col xs={4}>
                                        <Button block size="md" type="submit">Submit</Button>
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                    </div>
                }
            />
        )
    }
}