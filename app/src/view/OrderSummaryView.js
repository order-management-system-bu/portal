import React from "react";
import {authGet, RmwEndpoints} from "../utility/request-handler";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import {beutifyString} from "../utility/string-functions";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";

export default class OrderSummaryView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            orderStatus: "",
            price: "",

            discountCode: "",
            customerEmail: "",
            customerName: "",
            customerContactNumber: "",

            street: "",
            city: "",
            county: "",
            postcode: ""
        }
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        authGet(RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN, (customerResponse) => {
                authGet(
                    RmwEndpoints.Om.VIEW_ORDER(params.orderId),
                    (response) => {
                        if (response.data["customerId"] === customerResponse.data["id"]) {
                            this.setState({
                                orderStatus: response.data["orderStatus"],
                                price: response.data["price"].toFixed(2),
                                discountCode: response.data["discountCode"],
                                customerEmail: response.data["customerEmail"],
                                customerName: response.data["customerName"],
                                customerContactNumber: response.data["customerContactNumber"],
                                street: response.data["street"],
                                city: response.data["city"],
                                county: response.data["county"],
                                postcode: response.data["postcode1"] + " " + response.data["postcode2"]
                            });
                        }
                    }
                );
            }
        );
    }

    render() {
        return (
            <Card className="width-handler margin-top-l padding-bottom-l">
                <Card.Body>
                    {!this.state.orderStatus ?
                        <Row>
                            <Col>
                                <h1>Failed to retrieve order details</h1>
                            </Col>
                        </Row> : <Container>
                            <Row>
                                <Col>
                                    <h4>Order Summary</h4>
                                </Col>
                            </Row>
                            {["discountCode", "orderStatus"].map(name =>
                                this.state[name] === "" ? null :
                                    <Row>
                                        <Col xs={4}>
                                            <p>{beutifyString(name)}</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>{this.state[name]}</p>
                                        </Col>
                                    </Row>
                            )}
                            <br/>
                            <Row>
                                <Col>
                                    <h4>Customer Information</h4>
                                </Col>
                            </Row>
                            {["customerName", "customerEmail", "customerContactNumber"].map(name =>
                                this.state[name] === "" ? null :
                                    <Row>
                                        <Col xs={4}>
                                            <p>{beutifyString(name)}</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>{this.state[name]}</p>
                                        </Col>
                                    </Row>
                            )}
                            <br/>
                            <Row>
                                <Col>
                                    <h4>Address Information</h4>
                                </Col>
                            </Row>
                            {["street", "city", "county", "postcode"].map(name =>
                                this.state[name] === "" ? null :
                                    <Row>
                                        <Col xs={4}>
                                            <p>{beutifyString(name)}</p>
                                        </Col>
                                        <Col xs={4}>
                                            <p>{this.state[name]}</p>
                                        </Col>
                                    </Row>
                            )}
                        </Container>
                    }
                </Card.Body>
            </Card>
        )
    }
}