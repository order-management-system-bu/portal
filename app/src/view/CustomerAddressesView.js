import React from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import CustomerControlNavbarWrapper from "./component/CustomerControlNavbarWrapper";
import {cleanupFormMessages, handleChange} from "../utility/form-functions";
import {authDelete, authGet, authPost, authPut, RmwEndpoints} from "../utility/request-handler";
import {FailureAlert, SuccessAlert} from "./component/DismissibleAlert";
import {beutifyString} from "../utility/string-functions";

/**
 * Function is responsible for rendering the CustomerAddresses view.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class CustomerAddressesView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: null,
            addresses: [],

            customerId: "",
            street: "",
            city: "",
            county: "",
            postcode1: "",
            postcode2: ""
        }
    }

    componentDidMount() {
        this.fetchStoredAddresses();
    }

    fetchStoredAddresses() {
        if (this.state.customerId === "") {
            authGet(RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN, (customerResponse) => {
                    this.setState({
                        customerId: customerResponse.data["id"]
                    });
                    authGet(RmwEndpoints.Urm.LIST_ADDRESS_BY_CUSTOMER_ID(customerResponse.data["id"]), (addressResponse) => this.setState({
                        addresses: addressResponse.data
                    }))
                }
            )
        } else {
            authGet(RmwEndpoints.Urm.LIST_ADDRESS_BY_CUSTOMER_ID(this.state.customerId), (addressResponse) => this.setState({
                addresses: addressResponse.data
            }))
        }
    }

    handleAssignDefaultAddress = (event, addressId) => {
        event.preventDefault();
        cleanupFormMessages();
        authPut(
            RmwEndpoints.Urm.ASSIGN_ACTIVE_ADDRESS(addressId),
            null,
            () => {
                this.setState({
                    alert: <SuccessAlert messages="Successfully changed your default address." component={this}/>
                });
                this.fetchStoredAddresses();
            },
            event.target
        );
    };

    handleDeleteAddress = (event, addressId) => {
        event.preventDefault();
        authDelete(
            RmwEndpoints.Urm.REMOVE_ADDRESS(addressId),
            () => {
                this.setState({
                    alert: <SuccessAlert messages="Successfully removed the address." component={this}/>
                });
                this.fetchStoredAddresses();
            },
            (error) => {
                const defaultError = "Failed to remove the address.";
                this.setState({
                    alert: <FailureAlert
                        messages={error.response.data ? error.response.data.map(vf => vf["fault"]) : defaultError}
                        component={this}
                    />
                });
                this.fetchStoredAddresses();
            })
    };

    handleCreateAddress = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        authPost(
            RmwEndpoints.Urm.NEW_ADDRESS,
            {
                customerId: this.state.customerId,
                street: this.state.street,
                city: this.state.city,
                county: this.state.county,
                postcode1: this.state.postcode1,
                postcode2: this.state.postcode2
            },
            () => {
                this.setState({
                    alert: <SuccessAlert messages="Successfully stored your new default address." component={this}/>
                });
                this.fetchStoredAddresses();
            },
            event.target
        );
    };

    render() {
        return (
            <CustomerControlNavbarWrapper
                activeKey="addressesView"
                pageContent={
                    <div>
                        {this.state.alert}
                        <Row>
                            <Col>
                                <h4 className="font-black font-xl text-center">
                                    Your Delivery Addresses
                                </h4>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            {this.state.addresses.map(address =>
                                <AddressView
                                    address={address}
                                    handleDeleteAddress={this.handleDeleteAddress}
                                    handleAssignDefaultAddress={this.handleAssignDefaultAddress}/>
                            )}
                        </Row>
                        <hr/>
                        <NewAddressForm
                            alert={this.state.alert}
                            handleCreateAddress={this.handleCreateAddress}
                            component={this}/>
                    </div>
                }
            />
        )
    }
}

const AddressView = (props) =>
    <Col className="padding-m" xs={4}>
        <Card>
            {!props["address"]["defaultAddress"] ? null :
                <Card.Header>
                    <Card.Title className="margin-none">Default Address</Card.Title>
                </Card.Header>
            }
            <Card.Body>
                {[props["address"]["street"], props["address"]["city"], props["address"]["postcode1"] + " " + props["address"]["postcode2"]].map(element =>
                    <Card.Text className="margin-xs">
                        {element}
                    </Card.Text>
                )}
                {props["address"]["defaultAddress"] ? null :
                    <Row>
                        <Col xs={6}>
                            <Form onSubmit={e => props.handleDeleteAddress(e, props["address"]["id"])}>
                                <Button type="submit" variant="danger" size="sm" block>Remove</Button>
                            </Form>
                        </Col>
                        <Col xs={6}>
                            <Form onSubmit={e => props.handleAssignDefaultAddress(e, props["address"]["id"])}>
                                <Button type="submit" size="sm" block>Assign Default</Button>
                            </Form>
                        </Col>
                    </Row>
                }
            </Card.Body>
        </Card>
    </Col>;

const NewAddressForm = (props) =>
    <div>
        <Row>
            <Col>
                <h4 className="font-black font-l">
                    Add a new delivery address:
                </h4>
            </Col>
        </Row>
        <Form onSubmit={e => props.handleCreateAddress(e)}>
            {["street", "city", "county"].map(name =>
                <Row className="margin-top-s">
                    <Col xs={4}>
                        <Form.Group onChange={e => handleChange(e, props["component"])}>
                            <Form.Label className="font-black font-s">
                                {beutifyString(name)}
                            </Form.Label>
                            <Form.Control id={name} size="sm" type="text" maxlength="30"/>
                        </Form.Group>
                    </Col>
                </Row>
            )}
            <Row className="margin-top-s">
                {["postcode1", "postcode2"].map(name =>
                    <Col xs={2}>
                        <Form.Group onChange={e => handleChange(e, props["component"])}>
                            <Form.Label className="font-black font-s">
                                <b>{beutifyString(name)}</b>
                            </Form.Label>
                            <Form.Control id={name} size="sm" type="text" maxlength="4"/>
                        </Form.Group>
                    </Col>
                )}
            </Row>
            <Row className="margin-top-s">
                <Col xs={4}>
                    <Button block size="md" type="submit">Submit</Button>
                </Col>
            </Row>
        </Form>
    </div>;