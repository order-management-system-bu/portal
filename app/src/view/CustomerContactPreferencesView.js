import React from "react";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import CustomerControlNavbarWrapper from "./component/CustomerControlNavbarWrapper";
import {authGet, authPut, RmwEndpoints} from "../utility/request-handler";
import {SuccessAlert} from "./component/DismissibleAlert";
import {cleanupFormMessages} from "../utility/form-functions";

/**
 * Class is responsible for rendering the CustomerContactPreferencesView.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class CustomerContactPreferencesView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            customerId: "",
            receiveOrderUpdates: null,
            receiveNewsAndOffersSms: null,
            receiveNewsAndOffersEmail: null,
            receiveAccountNotifications: null
        }
    }

    componentDidMount() {
        authGet(RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN, (response) => this.setState({
            customerId: response.data["id"],
            receiveOrderUpdates: response.data["receiveOrderUpdates"],
            receiveNewsAndOffersSms: response.data["receiveNewsAndOffersSms"],
            receiveNewsAndOffersEmail: response.data["receiveNewsAndOffersEmail"],
            receiveAccountNotifications: response.data["receiveAccountNotifications"]
        }))
    }

    handleFormSubmit = (event) => {
        event.preventDefault();
        cleanupFormMessages();
        authPut(
            RmwEndpoints.Urm.UPDATE_CUSTOMER(this.state.customerId),
            {
                receiveOrderUpdates: this.state.receiveOrderUpdates,
                receiveNewsAndOffersSms: this.state.receiveNewsAndOffersSms,
                receiveNewsAndOffersEmail: this.state.receiveNewsAndOffersEmail,
                receiveAccountNotifications: this.state.receiveAccountNotifications
            },
            () => this.setState({
                alert: <SuccessAlert messages="Successfully updated your notification preferences." component={this}/>
            }),
            event.target
        );
    };

    render() {
        const handleChange = (event) => this.setState({[event.target.id]: event.target.checked});

        return (
            <CustomerControlNavbarWrapper
                activeKey="notificationsView"
                pageContent={
                    <div>
                        {this.state.alert}
                        <Row>
                            <Col>
                                <h4 className="font-black font-xl text-center">Notification Preferences</h4>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col xs={6}>
                                <Form onSubmit={e => this.handleFormSubmit(e)}>
                                    <h4>Receive account updates</h4>
                                    <label className="checkbox-container">
                                        Email
                                        <input id="receiveAccountNotifications" type="checkbox"
                                               checked={this.state.receiveAccountNotifications}
                                               onClick={e => handleChange(e)}/>
                                        <span className="checkbox-checkmark"/>
                                    </label>
                                    <br/>
                                    <h4>Receive order updates</h4>
                                    <label className="checkbox-container">
                                        Email
                                        <input id="receiveOrderUpdates" type="checkbox"
                                               checked={this.state.receiveOrderUpdates}
                                               onClick={e => handleChange(e)}/>
                                        <span className="checkbox-checkmark"/>
                                    </label>
                                    <br/>
                                    <h4>Receive news and discounts</h4>
                                    <label className="checkbox-container">
                                        Email
                                        <input id="receiveNewsAndOffersEmail" type="checkbox"
                                               checked={this.state.receiveNewsAndOffersEmail}
                                               onClick={e => handleChange(e)}/>
                                        <span className="checkbox-checkmark"/>
                                    </label>
                                    <label className="checkbox-container">
                                        Text message
                                        <input id="receiveNewsAndOffersSms" type="checkbox"
                                               checked={this.state.receiveNewsAndOffersSms}
                                               onClick={e => handleChange(e)}/>
                                        <span className="checkbox-checkmark"/>
                                    </label>
                                    <br/>
                                    <Button type="submit" variant="primary" size="lg">
                                        Save Changes
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </div>
                }
            />
        )
    }
}