import React from "react";
import Redirect from "react-router-dom/es/Redirect";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import AddToBasketModal from "./modal/AddToBasketModal";
import {requestGet, RmwEndpoints} from "../utility/request-handler";
import {beutifyString} from "../utility/string-functions";
import "../styles/menuview.css";
import CheckoutModal from "./modal/CheckoutModal";

/**
 * Class is responsible for rendering the Menu view.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class MenuView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectUrl: null,

            addToBasketModal: null,
            showAddToBasketModal: false,
            showCheckoutModal: false,
            deliveryFee: null,
            minimumForDelivery: null,

            menu: [],
            basket: []
        }
    }

    redirectToOrderSummary = (orderId) => this.setState({
        redirectUrl: "/order/summary/" + orderId
    });

    componentDidMount() {
        if (this.state.deliveryFee === null && this.state.minimumForDelivery === null) {
            requestGet(RmwEndpoints.Om.VIEW_DELIVERY_OPTION, (response) => this.setState({
                deliveryFee: response.data["deliveryCharge"],
                minimumForDelivery: response.data["minimumForDelivery"]
            }), () => this.setState({
                deliveryFee: 0.00,
                minimumForDelivery: 0.00
            }))
        }
        if (this.state.menu === undefined || this.state.menu.length === 0) {
            requestGet(RmwEndpoints.Om.LIST_MENU_CATEGORY, (response) => response.data.map(category =>
                requestGet(RmwEndpoints.Om.LIST_MENU_ITEM(category["id"]), (response) => this.setState({
                    menu: this.state.menu.concat({
                        title: category["title"],
                        description: category["description"],
                        menuItems: response.data
                    })
                }))
            ))
        }
    }

    showAddToBasketModal = (menuItem) => {
        const addToBasket = (menuItem) => {
            this.setState({
                basket: this.state.basket.concat([{
                    menuItemId: menuItem["menuItemId"],
                    title: menuItem["title"],
                    price: menuItem["price"],
                    selectedOptions: menuItem["selectedOptions"] !== undefined || menuItem["selectedOptions"] > 0 ?
                        menuItem["selectedOptions"] : []
                }])
            });
        };

        if ((menuItem["additionalOptionIds"] === undefined || menuItem["additionalOptionIds"].length === 0) &&
            (menuItem["additionalOptionGroupIds"] === undefined || menuItem["additionalOptionGroupIds"].length === 0)) {
            addToBasket({
                menuItemId: menuItem["id"],
                title: menuItem["title"],
                price: menuItem["price"],
                selectedOptions: []
            });
        } else {
            this.setState({
                showAddToBasketModal: true,
                addToBasketModal: <AddToBasketModal
                    menuItem={menuItem}
                    show={true}
                    onHide={() => this.setState({
                        addToBasketModal: null,
                        showAddToBasketModal: false
                    })}
                    addToBasket={addToBasket}
                />,
            });
        }
    };

    render() {
        const calculateSubtotal = () => {
            let subtotal = 0.0;
            if (this.state.basket === undefined || this.state.basket.length === 0)
                return subtotal;
            this.state.basket.forEach(basketItem => {
                if (basketItem["selectedOptions"] !== undefined || basketItem["selectedOptions"].length > 0)
                    basketItem["selectedOptions"].forEach(option => subtotal += option["price"]);
                subtotal += basketItem["price"]
            });
            return subtotal;
        };

        const calculateBasketItemPrice = (basketItem) => {
            let menuItemPrice = 0.0;
            if (basketItem["selectedOptions"] !== undefined || basketItem["selectedOptions"].length > 0)
                basketItem["selectedOptions"].forEach(option => menuItemPrice += option["price"]);
            return menuItemPrice + basketItem["price"];
        };

        const removeFromBasket = (index) => this.setState({
            basket: this.state.basket.filter(bi => bi !== this.state.basket[index])
        });

        return (
            this.state.redirectUrl ? <Redirect to={this.state.redirectUrl}/> :
                <Container className="padding-bottom-l">
                    <Row className="width-handler margin-top-l">
                        <Col xs={7}>
                            <Card>
                                <Card.Body>
                                    {this.state.menu.map(category =>
                                        <div>
                                            <h4 id={category["title"]} className="font-black font-xl">
                                                {beutifyString(category["title"])}
                                            </h4>
                                            {category["description"] ? <p>{category["description"]}</p> : null}
                                            <ul className="menu-item-wrapper">
                                                {category["menuItems"].map(menuItem =>
                                                    <li className="menu-item"
                                                        onClick={() => this.showAddToBasketModal(menuItem)}>
                                                        <Card className="margin-xs">
                                                            <Card.Body>
                                                                <Card.Title
                                                                    className="font-black font-m">{menuItem["title"]}
                                                                </Card.Title>
                                                                <Card.Subtitle
                                                                    className="font-grey font-s padding-top-s">{menuItem["description"]}
                                                                </Card.Subtitle>
                                                                <Card.Text className="font-grey font-m padding-top-s">
                                                                    {"£" + (menuItem["price"]).toFixed(2)}
                                                                </Card.Text>
                                                            </Card.Body>
                                                        </Card>
                                                    </li>
                                                )}
                                            </ul>
                                        </div>
                                    )}
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col xs={5}>
                            <Card className="basket">
                                <Card.Header>
                                    <Button type="button" variant="success" size="md" block
                                            onClick={() => this.setState({showCheckoutModal: true})}>
                                        Checkout
                                    </Button>
                                </Card.Header>
                                <Card.Body>
                                    {this.state.basket === undefined || this.state.basket.length === 0 ? null :
                                        <div>
                                            <div>
                                                <Row className="padding-left-s padding-right-s">
                                                    <Col xs={6}><b>Product</b></Col>
                                                    <Col xs={3}><b>Price</b></Col>
                                                </Row>
                                                {this.state.basket.map((basketItem, index) => {
                                                    return (
                                                        <div>
                                                            <Row className="padding-left-s padding-right-s">
                                                                <Col xs={6}>{basketItem["title"]}</Col>
                                                                <Col xs={4}>
                                                                    £{(calculateBasketItemPrice(this.state.basket[index])).toFixed(2)}
                                                                </Col>
                                                                <Col xs={2}>
                                                                    <i className="fas fa-trash-alt btn-link-delete"
                                                                       onClick={() => removeFromBasket(index)}
                                                                    />
                                                                </Col>
                                                            </Row>
                                                            {basketItem["selectedOptions"] === undefined || basketItem["selectedOptions"].length === 0 ? null :
                                                                basketItem["selectedOptions"].map(option =>
                                                                    <Row
                                                                        className="padding-left-m padding-right-s">
                                                                        <Col className="font-grey font-s"
                                                                             xs={5}>
                                                                            {option["title"]}
                                                                        </Col>
                                                                    </Row>
                                                                )
                                                            }
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                            <div className="margin-top-m">
                                                <hr/>
                                                <Row className="padding-left-s padding-right-s">
                                                    <Col xs={8}><b>Subtotal</b></Col>
                                                    <Col xs={4}>{"£" + calculateSubtotal().toFixed(2)}</Col>
                                                </Row>
                                                <Row className="padding-left-s padding-right-s">
                                                    <Col xs={8}><b>Delivery Charge</b></Col>
                                                    <Col xs={4}>{"£" + this.state.deliveryFee.toFixed(2)}</Col>
                                                </Row>
                                                <Row className="padding-left-s padding-right-s">
                                                    <Col xs={8}><b>Total</b></Col>
                                                    <Col xs={4}>
                                                        {"£" + (calculateSubtotal() + this.state.deliveryFee).toFixed(2)}
                                                    </Col>
                                                </Row>
                                            </div>
                                        </div>
                                    }
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    {this.state.addToBasketModal}
                    <CheckoutModal
                        show={this.state.showCheckoutModal}
                        onHide={() => this.setState({showCheckoutModal: false})}
                        basket={this.state.basket}
                        deliveryFee={this.state.deliveryFee}
                        minimumForDelivery={this.state.minimumForDelivery}
                        calculateSubtotal={() => calculateSubtotal()}
                        calculateBasketItemPrice={(basketItem) => calculateBasketItemPrice(basketItem)}
                        redirectToOrderSummary={(orderId) => this.redirectToOrderSummary(orderId)}
                    />
                </Container>
        )
    }
}