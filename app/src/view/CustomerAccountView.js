import React from "react";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import {DeleteCustomerModal} from "./modal/DeleteCustomerModal";
import UpdateCustomerModal from "./modal/UpdateCustomerModal";
import CustomerControlNavbarWrapper from "./component/CustomerControlNavbarWrapper";
import {authGet, RmwEndpoints} from "../utility/request-handler";
import {beutifyString} from "../utility/string-functions";
import UpdateCustomerPasswordModal from "./modal/UpdateCustomerPasswordModal";

/**
 * Class is responsible for rendering the CustomerAccountView.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
export default class CustomerAccountView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: null,
            showUpdateCustomerModal: false,
            showDeleteCustomerModal: false,
            showUpdateCustomerPasswordModal: false,

            customerId: "",
            username: "",
            forename: "",
            surname: "",
            contactNumber: ""
        }
    }

    showModal = (stateId) => this.setState({[stateId]: true});

    hideModal = (stateId) => this.setState({[stateId]: false});

    componentDidMount() {
        authGet(RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN, (response) => this.setState({
            customerId: response.data["id"],
            username: response.data["username"],
            forename: response.data["forename"],
            surname: response.data["surname"],
            contactNumber: response.data["contactNumber"]
        }));
    }

    render() {
        return (
            <CustomerControlNavbarWrapper
                activeKey="accountView"
                pageContent={
                    <div>
                        {this.state.alert}
                        <Row>
                            <Col>
                                <h4 className="font-black font-xl text-center">Your Account</h4>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col cs={6}>
                                <Row>
                                    <Col xs={3}><b>Email Address:</b></Col>
                                    <Col xs={8}>{this.state.username}</Col>
                                </Row>
                                {["contactNumber", "forename", "surname"].map(name =>
                                    <Row>
                                        <Col xs={3}><b>{beutifyString(name)}:</b></Col>
                                        <Col xs={8}>{this.state[name]}</Col>
                                    </Row>
                                )}
                            </Col>
                        </Row>
                        <hr/>
                        <Row>
                            <Col xs={4}>
                                <Button type="button" variant="primary" size="md" block
                                        onClick={() => this.showModal("showUpdateCustomerModal")}>
                                    Update Details
                                </Button>
                            </Col>
                            <Col xs={4}>
                                <Button type="button" variant="danger" size="md" block
                                        onClick={() => this.showModal("showDeleteCustomerModal")}>
                                    Delete Account
                                </Button>
                            </Col>
                            <Col xs={4}>
                                <Button type="button" variant="primary" size="md" block
                                        onClick={() => this.showModal("showUpdateCustomerPasswordModal")}>
                                    Update Password
                                </Button>
                            </Col>
                        </Row>
                        <UpdateCustomerModal
                            show={this.state.showUpdateCustomerModal}
                            onHide={() => this.hideModal("showUpdateCustomerModal")}
                            customerId={this.state.customerId}
                            forename={this.state.forename}
                            surname={this.state.surname}
                            contactNumber={this.state.contactNumber}
                        />
                        <DeleteCustomerModal
                            show={this.state.showDeleteCustomerModal}
                            onHide={() => this.hideModal("showDeleteCustomerModal")}
                            customerId={this.state.customerId}
                        />
                        <UpdateCustomerPasswordModal
                            show={this.state.showUpdateCustomerPasswordModal}
                            onHide={() => this.hideModal("showUpdateCustomerPasswordModal")}
                            customerId={this.state.customerId}
                        />
                    </div>
                }
            />
        )
    }
}