import React from "react";
import ReactDOM from "react-dom";
import Nav from "react-bootstrap/Nav";
import MenuView from "./view/MenuView";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import SignInModal from "./view/modal/SignInModal";
import SignUpModal from "./view/modal/SignUpModal";

import BrowserRouter from "react-router-dom/es/BrowserRouter";
import Link from "react-router-dom/es/Link";
import Route from "react-router-dom/es/Route";
import Switch from "react-router-dom/es/Switch";
import Redirect from "react-router-dom/es/Redirect";

import CustomerAccountView from "./view/CustomerAccountView";
import CustomerOrdersView from "./view/CustomerOrdersView";
import CustomerCardsView from "./view/CustomerCardsView";
import CustomerAddressesView from "./view/CustomerAddressesView";
import CustomerContactPreferencesView from "./view/CustomerContactPreferencesView";
import OrderSummaryView from "./view/OrderSummaryView";

import "./styles/index.css";
import "./styles/padding.css";
import "./styles/margin.css";
import "./styles/fonts.css";
import "./styles/button.css";
import "./styles/text.css";
import "./styles/form.css";

import {beutifyString} from "./utility/string-functions";
import {authGet, requestGet, RmwEndpoints} from "./utility/request-handler";

/**
 * Class is responsible for handling app-level decisions.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSignInModal: false,
            showSignUpModal: false,

            customer: null,
            categories: []
        };
    }

    componentDidMount() {
        this.fetchCustomerByAuthToken();
        requestGet(
            RmwEndpoints.Om.LIST_MENU_CATEGORY,
            (response) => this.setState({
                categories: response.data
            }));
    }

    fetchCustomerByAuthToken = () => {
        authGet(
            RmwEndpoints.Urm.VIEW_CUSTOMER_BY_AUTH_TOKEN,
            (response) => this.setState({
                customer: response.data
            })
        )
    };

    showModal = (stateId) => this.setState({[stateId]: true});

    hideModal = (stateId) => this.setState({[stateId]: false});

    showSignUpModal = () => this.setState({
        showSignInModal: false,
        showSignUpModal: true
    });

    signOut = () => {
        localStorage.removeItem("oms.authentication.key");
        this.setState({customer: null})
    };

    render() {
        return (
            <BrowserRouter>
                <div>
                    <div className="navbar-wrapper sticky-top">
                        <Navbar className="width-handler" collapseOnSelect expand="lg" bg="light" variant="light">
                            <Navbar.Brand>
                                <Nav.Item>
                                    <Link to="/">Menu</Link>
                                </Nav.Item>
                            </Navbar.Brand>
                            <Navbar.Toggle aria-controls="navbar-collapse"/>
                            <Navbar.Collapse id="navbar-collapse">
                                <Nav className="mr-auto">
                                    {this.state.categories.map(category =>
                                        <Nav.Item>
                                            <Nav.Link href={"/#" + category["title"]}>
                                                {category["title"]}
                                            </Nav.Link>
                                        </Nav.Item>
                                    )}
                                </Nav>
                                <Nav className="justify-content-end">
                                    {this.state.customer === null ?
                                        <Nav.Link onClick={() => this.showModal("showSignInModal")}>
                                            Sign In
                                        </Nav.Link> :
                                        <NavDropdown
                                            title={beutifyString(this.state.customer["forename"]) + " " + beutifyString(this.state.customer["surname"])}>
                                            <NavDropdown.Item>
                                                <Link to="/customer/update">Your Account</Link>
                                            </NavDropdown.Item>
                                            <NavDropdown.Item>
                                                <Link to="/customer/orders">Your Orders</Link>
                                            </NavDropdown.Item>
                                            <NavDropdown.Item>
                                                <Link to="/customer/cards">Your Saved Cards</Link>
                                            </NavDropdown.Item>
                                            <NavDropdown.Item>
                                                <Link to="/customer/addresses">Your Delivery Addresses</Link>
                                            </NavDropdown.Item>
                                            <NavDropdown.Item>
                                                <Link to="/customer/preferences">Contact Preferences</Link>
                                            </NavDropdown.Item>
                                            <NavDropdown.Divider/>
                                            <NavDropdown.Item onClick={() => this.signOut()}>Log Out</NavDropdown.Item>
                                        </NavDropdown>
                                    }
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                    </div>
                    <SignInModal
                        show={this.state.showSignInModal}
                        onHide={() => this.hideModal("showSignInModal")}
                        showSignUpModal={() => this.showSignUpModal()}
                        fetchCustomerByAuthToken={() => this.fetchCustomerByAuthToken()}
                    />
                    <SignUpModal
                        show={this.state.showSignUpModal}
                        onHide={() => this.hideModal("showSignUpModal")}
                    />
                    <Switch>
                        <Route exact path="/" component={MenuView}/>
                        <Route exact path="/order/summary/:orderId" component={OrderSummaryView}/>

                        <PrivateRoute path="/customer/update" component={CustomerAccountView}/>
                        <PrivateRoute path="/customer/orders" component={CustomerOrdersView}/>
                        <PrivateRoute path="/customer/cards" component={CustomerCardsView}/>
                        <PrivateRoute path="/customer/addresses" component={CustomerAddressesView}/>
                        <PrivateRoute path="/customer/preferences" component={CustomerContactPreferencesView}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

/**
 * Function represents a single private route in the applications
 * routing schema.
 *
 * @author Liam McGuire
 * @version 0.1.0
 */
const PrivateRoute = (props) =>
    localStorage.getItem("oms.authentication.key") !== null ?
        <Route exact path={props["path"]} component={props["component"]}/> :
        <Redirect to="/"/>;


ReactDOM.render(<App/>, document.getElementById("root"));
